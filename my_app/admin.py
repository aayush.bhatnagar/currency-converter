from django.contrib import admin

from my_app.models import Currency


class CurrencyAdmin(admin.ModelAdmin):
    pass


admin.site.register(Currency, CurrencyAdmin)
