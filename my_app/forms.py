from django import forms

from my_app.models import Currency


def unique_currency():
    currency_list = []
    for obj in Currency.objects.filter():
        currency_list.append(obj.currency_title)
    x = []
    x.append(("", "Select Currency"))
    for i in currency_list:
        x.append((i, i))
    return x


class CurrencyForm(forms.Form):
    currency = forms.CharField(
        max_length=2000,
        label="Select Currency",
        widget=forms.Select(choices=unique_currency())
    )
