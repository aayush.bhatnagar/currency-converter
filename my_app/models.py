from django.db import models


class Currency(models.Model):
    currency_title = models.CharField(max_length=255, verbose_name="Currency")

    class Meta:
        verbose_name = "Currency"
        ordering = ['currency_title']

    def __str__(self):
        return self.currency_title
