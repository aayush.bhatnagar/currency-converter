import json

from django.shortcuts import render
from django.views.generic.edit import FormMixin
from django.urls import reverse_lazy
from django.views import View
import requests

from my_app.forms import CurrencyForm
from my_app.models import Currency


class Home(View, FormMixin):
    model = Currency
    template_name = 'home.html'
    context_object_name = 'currency'
    form_class = CurrencyForm

    def get_success_url(self):
        return reverse_lazy('home')

    def get_queryset(self, **kwargs):
        form = self.form_class(self.request.GET)
        if form.is_valid():
            q = self.model.objects.filter()
            return q
        return self.model.objects.all()

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.GET)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        selcted_currency = form.data.get('currency')
        custom_context = None
        if selcted_currency:
            url = 'https://api.exchangeratesapi.io/latest?base=%s' % (
                selcted_currency)
            headers = {
                'Content-Type': 'application/json'
            }
            data = requests.get(url, headers=headers)
            custom_context = json.loads(data.content).get('rates')
            try:
                custom_context.pop(selcted_currency)
            except KeyError:
                pass
        return render(request, 'convert.html', {"data": custom_context})
