from django.urls import path

from my_app.views import Home

urlpatterns = [
    path('', Home.as_view(), name='home')
]
