# Currency Converter

#### Requirements :
</br>
<li> Python 3.6 </li>
</br></br>

#### Steps to run project:
</br>
<li> Create virtual env.</li>
<li> Install required packages from requirements.txt using </li>


```python
    pip install -r requirements.txt
```


<li> Start server </li>


```python
    python manage.py runserver
```

#### Steps to convert currency:
</br>
<li> After successfully starting project, visit <strong>http://localhost:8000/converter/ </strong>in your browser.</li>

<li> Select required currency which you want to convert.</li>

<li> Click on convert button.</li>
